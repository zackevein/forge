local sin, cos, atan, atan2, sqrt, rad = math.sin, math.cos, math.atan, math.atan2, math.sqrt, math.rad
local tinsert, tremove = tinsert, tremove

-- patch WorldToScreen for 6.2 / 2.1.2 update
local function WorldToScreen (wX, wY, wZ)
    local onScreen, sX, sY = env.WorldToScreen(wX, wY, wZ)
    if not onScreen then return nil, nil end

    if sX and sY then
        return WorldFrame:GetRight() * sX, -(WorldFrame:GetTop() * (1 - sY))
    else
        return sX, sY
    end
end

local function Los(unit, other)
    if unit == nil then return false end
    other = other or "player"

    local x1, y1, z1 = ObjectPosition(other)
    local x2, y2, z2 = ObjectPosition(unit)

    if x2 == nil then return false end

    local result = TraceLine(x1, y1, z1+2.25, x2, y2, z2+2.25, bit.bor(0x10, 0x100, 0x1))

    return result == 0 or result == nil
end

local function InRange(unit, min, max, other)
    if unit == "player" then return true end
    if unit == nil then return false end
    other = other or "player"

    local xa, ya, za = ObjectPosition(other)
    local xb, yb, zb = ObjectPosition(unit)
    local x, y, z = xb - xa, yb - ya, zb - za

    if xa == nil or xb == nil then return false end

    yes = math.sqrt(x*x + y*y + z*z)

    if yes == nil then return end

    return yes >= min and yes < max
end

if LibStub then
    -- LibStub version control
    LibDraw = LibStub:NewLibrary("LibDraw-1.0", 1)
    if not LibDraw then return end
else
    -- Pretty much LibStub
    LibDraw = {
        version = 1.1
    }
    if _G['LibDraw'] and _G['LibDraw'].version and _G['LibDraw'].version > LibDraw.version then
        return
    else
        _G['LibDraw'] = LibDraw
    end
end

LibDraw.line = LibDraw.line or { r = 0, g = 1, b = 0, a = 1, w = 1 }
LibDraw.texture = "Interface\\FriendsFrame\\WowshareTextures.BLP"
LibDraw.level = "BACKGROUND"
LibDraw.callbacks = { }

if not LibDraw.canvas then
    LibDraw.canvas = CreateFrame("Frame", WorldFrame)
    LibDraw.canvas:SetAllPoints(WorldFrame)
    LibDraw.textures = { }
    LibDraw.textures_used = { }
    LibDraw.fontstrings = { }
    LibDraw.fontstrings_used = { }
end

function LibDraw.SetColor(r, g, b, a)
    LibDraw.line.r = r * 0.00390625
    LibDraw.line.g = g * 0.00390625
    LibDraw.line.b = b * 0.00390625
    if a then
        LibDraw.line.a = a * 0.01
    else
        LibDraw.line.a = 1
    end
end

function LibDraw.SetColorRaw(r, g, b, a)
    LibDraw.line.r = r
    LibDraw.line.g = g
    LibDraw.line.b = b
    LibDraw.line.a = a
end

function LibDraw.SetWidth(w)
    LibDraw.line.w = w
end

function LibDraw.Line(sx, sy, sz, ex, ey, ez)

    local sx, sy = WorldToScreen(sx, sy, sz)
    local ex, ey = WorldToScreen(ex, ey, ez)

    LibDraw.Draw2DLine(sx, sy, ex, ey)
end

function LibDraw.rotateX(cx, cy, cz, px, py, pz, r)
    if r == nil then return px, py, pz end
    local s = sin(r)
    local c = cos(r)
    -- center of rotation
    px, py, pz = px - cx,  py - cy, pz - cz
    local x = px + cx
    local y = ((py * c - pz * s) + cy)
    local z = ((py * s + pz * c) + cz)
    return x, y, z
end

function LibDraw.rotateY(cx, cy, cz, px, py, pz, r)
    if r == nil then return px, py, pz end
    local s = sin(r)
    local c = cos(r)
    -- center of rotation
    px, py, pz = px - cx,  py - cy, pz - cz
    local x = ((pz * s + px * c) + cx)
    local y = py + cy
    local z = ((pz * c - px * s) + cz)
    return x, y, z
end

function LibDraw.rotateZ(cx, cy, cz, px, py, pz, r)
    if r == nil then return px, py, pz end
    local s = sin(r)
    local c = cos(r)
    -- center of rotation
    px, py, pz = px - cx,  py - cy, pz - cz
    local x = ((px * c - py * s) + cx)
    local y = ((px * s + py * c) + cy)
    local z = pz + cz
    return x, y, z
end

function LibDraw.Array(vectors, x, y, z, rotationX, rotationY, rotationZ)
    for _, vector in ipairs(vectors) do
        local sx, sy, sz = x+vector[1], y+vector[2], z+vector[3]
        local ex, ey, ez = x+vector[4], y+vector[5], z+vector[6]

        if rotationX then
            sx, sy, sz = LibDraw.rotateX(x, y, z, sx, sy, sz, rotationX)
            ex, ey, ez = LibDraw.rotateX(x, y, z, ex, ey, ez, rotationX)
        end
        if rotationY then
            sx, sy, sz = LibDraw.rotateY(x, y, z, sx, sy, sz, rotationY)
            ex, ey, ez = LibDraw.rotateY(x, y, z, ex, ey, ez, rotationY)
        end
        if rotationZ then
            sx, sy, sz = LibDraw.rotateZ(x, y, z, sx, sy, sz, rotationZ)
            ex, ey, ez = LibDraw.rotateZ(x, y, z, ex, ey, ez, rotationZ)
        end

        local sx, sy = WorldToScreen(sx, sy, sz)
        local ex, ey = WorldToScreen(ex, ey, ez)
        LibDraw.Draw2DLine(sx, sy, ex, ey)
    end
end

local LINEFACTOR = 256/254;
local LINEFACTOR_2 = LINEFACTOR / 2;



function LibDraw.Draw2DLine(sx, sy, ex, ey)

    local T = tremove(LibDraw.textures) or false
    if T == false then
        T = LibDraw.canvas:CreateTexture(nil, "BACKGROUND")
        T:SetDrawLayer(LibDraw.level)
    end
    T:SetTexture(LibDraw.texture)
    tinsert(LibDraw.textures_used, T)

    -- Determine dimensions and center point of line
    local dx, dy = ex - sx, ey - sy
    local cx, cy = (sx + ex) * 0.5, (sy + ey) * 0.5

    -- Normalize direction if necessary
    if (dx < 0) then
        dx,dy = -dx, -dy
    end

    -- Calculate actual length of line
    local l = sqrt((dx * dx) + (dy * dy))

    -- Quick escape if it's zero length
    if (l == 0) then
        T:SetTexCoord(0,0,0,0,0,0,0,0)
        T:SetPoint("BOTTOMLEFT", C, "TOPLEFT", cx,cy)
        T:SetPoint("TOPRIGHT",   C, "TOPLEFT", cx,cy)
        return
    end

    -- Sin and Cosine of rotation, and combination (for later)
    local s,c = -dy / l, dx / l
    local sc = s * c

    -- Calculate bounding box size and texture coordinates
    local Bwid, Bhgt, BLx, BLy, TLx, TLy, TRx, TRy, BRx, BRy
    if (dy >= 0) then
        Bwid = ((l * c) - (LibDraw.line.w * s)) * LINEFACTOR_2
        Bhgt = ((LibDraw.line.w * c) - (l * s)) * LINEFACTOR_2
        BLx, BLy, BRy = (LibDraw.line.w / l) * sc, s * s, (l / LibDraw.line.w) * sc
        BRx, TLx, TLy, TRx = 1 - BLy, BLy, 1 - BRy, 1 - BLx
        TRy = BRx;
    else
        Bwid = ((l * c) + (LibDraw.line.w * s)) * LINEFACTOR_2
        Bhgt = ((LibDraw.line.w * c) + (l * s)) * LINEFACTOR_2
        BLx, BLy, BRx = s * s, -(l / LibDraw.line.w) * sc, 1 + (LibDraw.line.w / l) * sc
        BRy, TLx, TLy, TRy = BLx, 1 - BRx, 1 - BLx, 1 - BLy
        TRx = TLy
    end

    if TLx > 10000 then TLx = 10000 elseif TLx < -10000 then TLx = -10000 end
    if TLy > 10000 then TLy = 10000 elseif TLy < -10000 then TLy = -10000 end
    if BLx > 10000 then BLx = 10000 elseif BLx < -10000 then BLx = -10000 end
    if BLy > 10000 then BLy = 10000 elseif BLy < -10000 then BLy = -10000 end
    if TRx > 10000 then TRx = 10000 elseif TRx < -10000 then TRx = -10000 end
    if TRy > 10000 then TRy = 10000 elseif TRy < -10000 then TRy = -10000 end
    if BRx > 10000 then BRx = 10000 elseif BRx < -10000 then BRx = -10000 end
    if BRy > 10000 then BRy = 10000 elseif BRy < -10000 then BRy = -10000 end

    T:ClearAllPoints()

    -- Set texture coordinates and anchors
    T:SetTexCoord(TLx, TLy, BLx, BLy, TRx, TRy, BRx, BRy)
    T:SetPoint("BOTTOMLEFT", LibDraw.canvas, "TOPLEFT", cx - Bwid, cy - Bhgt)
    T:SetPoint("TOPRIGHT",   LibDraw.canvas, "TOPLEFT", cx + Bwid, cy + Bhgt)
    T:SetVertexColor(LibDraw.line.r, LibDraw.line.g, LibDraw.line.b, LibDraw.line.a)
    T:Show()
end

local full_circle = rad(365)
local small_circle_step = rad(3)

function LibDraw.Circle(x, y, z, size)
    local lx, ly, nx, ny, fx, fy = false, false, false, false, false, false
    for v=0, full_circle, small_circle_step do
        nx, ny = WorldToScreen( (x+cos(v)*size), (y+sin(v)*size), z )
        LibDraw.Draw2DLine(lx, ly, nx, ny)
        lx, ly = nx, ny
    end
end

local flags = bit.bor(0x100)

function LibDraw.GroundCircle(x, y, z, size)
    local lx, ly, nx, ny, fx, fy, fz = false, false, false, false, false, false, false
    for v=0, full_circle, small_circle_step do
        fx, fy, fz = TraceLine(  (x+cos(v)*size), (y+sin(v)*size), z+100, (x+cos(v)*size), (y+sin(v)*size), z-100, flags )
        if fx == nil then
            fx, fy, fz = (x+cos(v)*size), (y+sin(v)*size), z
        end
        nx, ny = WorldToScreen( (fx+cos(v)*size), (fy+sin(v)*size), fz )
        LibDraw.Draw2DLine(lx, ly, nx, ny)
        lx, ly = nx, ny
    end
end

function LibDraw.Line(sx, sy, sz, ex, ey, ez)
    local sx, sy = WorldToScreen(sx, sy, sz)
    local ex, ey = WorldToScreen(ex, ey, ez)

    LibDraw.Draw2DLine(sx, sy, ex, ey)
end

function LibDraw.Arc(x, y, z, size, arc, rotation)
    local lx, ly, nx, ny, fx, fy = false, false, false, false, false, false
    local half_arc = arc * 0.5
    local ss = (arc/half_arc)
    local as, ae = -half_arc, half_arc
    for v = as, ae, ss do
        nx, ny = WorldToScreen( (x+cos(rotation+rad(v))*size), (y+sin(rotation+rad(v))*size), z )
        if lx and ly then
            LibDraw.Draw2DLine(lx, ly, nx, ny)
        else
            fx, fy = nx, ny
        end
        lx, ly = nx, ny
    end
    local px, py = WorldToScreen(x, y, z)
    LibDraw.Draw2DLine(px, py, lx, ly)
    LibDraw.Draw2DLine(px, py, fx, fy)
end

function LibDraw.Texture(config, x, y, z, alphaA)

    local texture, width, height = config.texture, config.width, config.height
    local left, right, top, bottom, scale =  config.left, config.right, config.top, config.bottom, config.scale
    local alpha = config.alpha or alphaA

    if not texture or not width or not height or not x or not y or not z then return end

    if not left or not right or not top or not bottom then
        left = 0
        right = 1
        top = 0
        bottom = 1
    end
    if not scale then
        local cx, cy, cz = GetCameraPosition()
        scale = width / LibDraw.Distance(x, y, z, cx, cy, cz)
    end

    local sx, sy = WorldToScreen(x, y, z)
    if not sx or not sy then return end
    local w = width * scale
    local h = height * scale
    sx = sx - w*0.5
    sy = sy + h*0.5
    local ex, ey = sx + w, sy - h

    local T = tremove(LibDraw.textures) or false
    if T == false then
        T = LibDraw.canvas:CreateTexture(nil, "BACKGROUND")
        T:SetDrawLayer(LibDraw.level)
        T:SetTexture(LibDraw.texture)
    end
    tinsert(LibDraw.textures_used, T)
    T:ClearAllPoints()
    T:SetTexCoord(left, right, top, bottom)
    T:SetTexture(texture)
    T:SetWidth(width)
    T:SetHeight(height)
    T:SetPoint("TOPLEFT", LibDraw.canvas, "TOPLEFT", sx, sy)
    T:SetPoint("BOTTOMRIGHT", LibDraw.canvas, "TOPLEFT", ex, ey)
    T:SetVertexColor(1, 1, 1, 1)
    if alpha then T:SetAlpha(alpha) else T:SetAlpha(1) end
    T:Show()

end

function LibDraw.Text(text, font, x, y, z)

    local sx, sy = WorldToScreen(x, y, z)

    if sx and sy then

        local F = tremove(LibDraw.fontstrings) or LibDraw.canvas:CreateFontString(nil, "BACKGROUND")

        F:SetFontObject(font)
        F:SetText(text)
        F:SetTextColor(LibDraw.line.r, LibDraw.line.g, LibDraw.line.b, LibDraw.line.a)

        F:SetPoint("TOPLEFT", UIParent, "TOPLEFT", sx, sy)
        --F:SetPoint("TOPLEFT", UIParent, "TOPLEFT", sx-(F:GetStringWidth()*0.5), sy)
        F:Show()

        tinsert(LibDraw.fontstrings_used, F)

    end

end

local rad90 = math.rad(-90)

function LibDraw.Box(x, y, z, width, height, rotation, offset_x, offset_y)

    if not offset_x then offset_x = 0 end
    if not offset_y then offset_y = 0 end

    if rotation then rotation = rotation + rad90 end

    local half_width = width * 0.5
    local half_height = height * 0.5

    local p1x, p1y = LibDraw.rotateZ(x, y, z, x - half_width + offset_x, y - half_width + offset_y, z, rotation)
    local p2x, p2y = LibDraw.rotateZ(x, y, z, x + half_width + offset_x, y - half_width + offset_y, z, rotation)
    local p3x, p3y = LibDraw.rotateZ(x, y, z, x - half_width + offset_x, y + half_width + offset_y, z, rotation)
    local p4x, p4y = LibDraw.rotateZ(x, y, z, x - half_width + offset_x, y - half_width + offset_y, z, rotation)
    local p5x, p5y = LibDraw.rotateZ(x, y, z, x + half_width + offset_x, y + half_width + offset_y, z, rotation)
    local p6x, p6y = LibDraw.rotateZ(x, y, z, x + half_width + offset_x, y - half_width + offset_y, z, rotation)
    local p7x, p7y = LibDraw.rotateZ(x, y, z, x - half_width + offset_x, y + half_width + offset_y, z, rotation)
    local p8x, p8y = LibDraw.rotateZ(x, y, z, x + half_width + offset_x, y + half_width + offset_y, z, rotation)

    LibDraw.Line(p1x, p1y, z, p2x, p2y, z)
    LibDraw.Line(p3x, p3y, z, p4x, p4y, z)
    LibDraw.Line(p5x, p5y, z, p6x, p6y, z)
    LibDraw.Line(p7x, p7y, z, p8x, p8y, z)

end

local deg45 = math.rad(45)
local arrowX = {
    { 0  , 0, 0, 1.5,  0,    0   },
    { 1.5, 0, 0, 1.2,  0.2, -0.2 },
    { 1.5, 0, 0, 1.2, -0.2,  0.2 }
}
local arrowY = {
    { 0, 0  , 0,  0  , 1.5,  0   },
    { 0, 1.5, 0,  0.2, 1.2, -0.2 },
    { 0, 1.5, 0, -0.2, 1.2,  0.2 }
}
local arrowZ = {
    { 0, 0, 0  ,  0,    0,   1.5 },
    { 0, 0, 1.5,  0.2, -0.2, 1.2 },
    { 0, 0, 1.5, -0.2,  0.2, 1.2 }
}

function LibDraw.DrawHelper()
    local playerX, playerY, playerZ = ObjectPosition("player")
    local old_red, old_green, old_blue, old_alpha, old_width = LibDraw.line.r, LibDraw.line.g, LibDraw.line.b, LibDraw.line.a, LibDraw.line.w

    -- X
    LibDraw.SetColor(255, 0, 0, 100)
    LibDraw.SetWidth(1)
    LibDraw.Array(arrowX, playerX, playerY, playerZ, deg45, false, false)
    LibDraw.Text('X', "GameFontNormal", playerX + 1.75, playerY, playerZ)
    -- Y
    LibDraw.SetColor(0, 255, 0, 100)
    LibDraw.SetWidth(1)
    LibDraw.Array(arrowY, playerX, playerY, playerZ, false, -deg45, false)
    LibDraw.Text('Y', "GameFontNormal", playerX, playerY + 1.75, playerZ)
    -- Z
    LibDraw.SetColor(0, 0, 255, 100)
    LibDraw.SetWidth(1)
    LibDraw.Array(arrowZ, playerX, playerY, playerZ, false, false, false)
    LibDraw.Text('Z', "GameFontNormal", playerX, playerY, playerZ + 1.75)

    LibDraw.line.r, LibDraw.line.g, LibDraw.line.b, LibDraw.line.a, LibDraw.line.w = old_red, old_green, old_blue, old_alpha, old_width
end

function LibDraw.Distance(ax, ay, az, bx, by, bz)
    return math.sqrt(((bx-ax)*(bx-ax)) + ((by-ay)*(by-ay)) + ((bz-az)*(bz-az)))
end

function LibDraw.Camera()
    local fX, fY, fZ = ObjectPosition("player")
    local sX, sY, sZ = GetCameraPosition()
    return sX, sY, sZ, atan2(sY - fY, sX - fX), atan((sZ - fZ) / sqrt(((fX - sX) ^ 2) + ((fY - sY) ^ 2)))
end

function LibDraw.Sync(callback)
    tinsert(LibDraw.callbacks, callback)
end

function LibDraw.clearCanvas()
    -- LibDraw.stats = #LibDraw.textures_used
    for i = #LibDraw.textures_used, 1, -1 do
        LibDraw.textures_used[i]:Hide()
        tinsert(LibDraw.textures, tremove(LibDraw.textures_used))
    end
    for i = #LibDraw.fontstrings_used, 1, -1 do
        LibDraw.fontstrings_used[i]:Hide()
        tinsert(LibDraw.fontstrings, tremove(LibDraw.fontstrings_used))
    end
end

local function OnUpdate()
    LibDraw.clearCanvas()
    for _, callback in ipairs(LibDraw.callbacks) do
        callback()
        if LibDraw.helper then
            LibDraw.DrawHelper()
        end
        LibDraw.helper = false
    end
end

function LibDraw.Enable(interval)
    local timer
    if not interval then
        timer = C_Timer.NewTicker(interval, OnUpdate)
    else
        timer = C_Timer.NewTicker(interval, OnUpdate)
    end
    return timer
end

spellIds={["Totem de séisme"]=8143,["Totem de glèbe"]=204336, ["Totem Fureur-du-ciel"]=204330}

LibDraw.Sync(function()
    local countWMB, isUpdatedWMB, addedObjectsWMB, removedObjectsWMB = GetObjectCount()

    local isarena = IsActiveBattlefieldArena()

    for i = 0,countWMB do
        local object = GetObjectWithIndex(i)
        if object and UnitIsPlayer(object) and (not isarena or (UnitIsUnit("party1", object) or UnitIsUnit("party2", object))) and not UnitIsUnit("player", object) then
            local x,y,z = ObjectPosition(object)

            local sc = 1.1
            if not isarena then
                sc = 0.5
            end

            local l, r, t, b = unpack(CLASS_ICON_TCOORDS[select(2, UnitClass(object))])
            local config = {
                texture = "Interface\\Glues\\CharacterCreate\\UI-CharacterCreate-Classes",
                left = l,
                right = r,
                top = t,
                bottom = b,
                width = 30,
                height = 30,
                scale = sc
            }
            local alpha = 0.4
            if Los(object) and InRange(object, 0, 43) then
                alpha = 1
            end
            LibDraw.Texture(config, x,y,z+2.25, alpha)
        end

        local name
        if object then
            name = UnitName(object)
        end

        if object and (name == "Totem de séisme" or name == "Totem de glèbe" or name == "Totem Fureur-du-ciel") then
            local x,y,z = ObjectPosition(object)

            local l, r, t, b = unpack(CLASS_ICON_TCOORDS[select(2, UnitClass(object))])
            local config = {
                texture = GetSpellTexture(spellIds[name]),
                width = 30,
                height = 30,
                scale = 0.9
            }
            local alpha = 0.4
            if Los(object) and InRange(object, 0, 43) then
                alpha = 1
            end
            LibDraw.Texture(config, x,y,z+2.25, alpha)
        end
    end
end)
LibDraw.Enable(0.01)
--LibDraw.canvas:SetScript("OnUpdate", OnUpdate)