local api = {}
local util = {
    ObjectClipMask = bit.bor(0x2, 0x4),
    BuildingClipMask = bit.bor(0x1),
    TerrainClipMask = bit.bor(0x8),
    FullClipMask = bit.bor(0x1, 0x2, 0x4, 0x8),

}
local state = {
    Climbing = 60,
    Flying = false,
    Clipping = {
        Object = false,
        Building = false,
        Terrain = false,
    },
    NameplateDistance = 30,
    CameraDistance = 30,
    CameraMaxDistance = 30,
    CameraMaxZoomFactor = 30,


}
local UI = {}

local updateClipMask = function()
    local clipMask = 0
    if state.Clipping.Object then
        clipMask = clipMask + util.ObjectClipMask
    end
    if state.Clipping.Building then
        clipMask = clipMask + util.BuildingClipMask
    end
    if state.Clipping.Terrain then
        clipMask = clipMask + util.TerrainClipMask
    end
    api.SetNoClipModes(clipMask)
end

for k,v in pairs(env) do
    api[k] = v
end

UI.UI = CreateFrame("Frame", "MonsHack", UIParent, "BackdropTemplate")

local ui = UI.UI

ui:SetPoint("TOP", UIParent, "TOP", 250, -5)
ui:SetSize(190, 270)
ui:SetBackdrop({
    bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
    edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Border",
    tile = true,
    tileSize = 16,
    edgeSize = 16,
    insets = {
        left = 5,
        right = 5,
        top = 5,
        bottom = 5
    }
})

-- Makes the UI movable
ui:EnableMouse(true)
ui:SetMovable(true)
ui:Hide()
ui:SetScript("OnMouseDown", function(self, button)
    if button == "LeftButton" then
        self:StartMoving()
    end
end)
ui:SetScript("OnMouseUp", function(self, button)
    if button == "LeftButton" then
        self:StopMovingOrSizing()
    end
end)


-- Create the logo texture
local logo = ui:CreateTexture(nil, "ARTWORK")
logo:SetTexture("C:\\Program Files (x86)\\World of Warcraft\\_classic_\\Interface\\MacroFrame\\MacroFrame-Icon")
logo:SetPoint("TOPLEFT", ui, "TOPLEFT", 10, -10)
logo:SetSize(48, 48)
local logoText = ui:CreateFontString(nil, "ARTWORK", "GameFontNormal")
logoText:SetPoint("LEFT", logo, "RIGHT", 5, 0)
logoText:SetText("Blacktech")


-- Create the clipping checkboxes
local objectClipMask = CreateFrame("CheckButton", "ObjectClipMask", ui, "UICheckButtonTemplate")
objectClipMask:SetPoint("TOPLEFT", logo, "BOTTOMLEFT", 0, -10)
objectClipMask:SetSize(20, 20)
objectClipMask:SetScript("OnClick", function(self)
    state.Clipping.Object = self:GetChecked()
    updateClipMask()
end)
local objectClipMaskText = objectClipMask:CreateFontString(nil, "ARTWORK", "GameFontNormal")
objectClipMaskText:SetPoint("LEFT", objectClipMask, "RIGHT", 5, 0)
objectClipMaskText:SetText("Objects")

-- Buildings
local buildingClipMask = CreateFrame("CheckButton", "ObjectClipMask", ui, "UICheckButtonTemplate")
buildingClipMask:SetPoint("TOPLEFT", logo, "BOTTOMLEFT", 0, -25)
buildingClipMask:SetSize(20, 20)
buildingClipMask:SetScript("OnClick", function(self)
    state.Clipping.Building = self:GetChecked()
    updateClipMask()
end)
local buildingClipMaskText = buildingClipMask:CreateFontString(nil, "ARTWORK", "GameFontNormal")
buildingClipMaskText:SetPoint("LEFT", buildingClipMask, "RIGHT", 5, 0)
buildingClipMaskText:SetText("Buildings")

-- Terrain
local terrainClipMask = CreateFrame("CheckButton", "ObjectClipMask", ui, "UICheckButtonTemplate")
terrainClipMask:SetPoint("TOPLEFT", logo, "BOTTOMLEFT", 0, -40)
terrainClipMask:SetSize(20, 20)
terrainClipMask:SetScript("OnClick", function(self)

end)
local terrainClipMaskText = terrainClipMask:CreateFontString(nil, "ARTWORK", "GameFontNormal")
terrainClipMaskText:SetPoint("LEFT", terrainClipMask, "RIGHT", 5, 0)
terrainClipMaskText:SetText("Terrain")



-- Create the flying checkbox
local flyingCheckbox = CreateFrame("CheckButton", "FlyingCheckbox", ui, "UICheckButtonTemplate")
flyingCheckbox:SetPoint("TOPLEFT", terrainClipMask, "BOTTOMLEFT", 0, -10)
flyingCheckbox:SetSize(20, 20)
flyingCheckbox:SetScript("OnClick", function(self)
    state.Flying = self:GetChecked()
    api.StopFalling()
    api.EnableFlyingMode(state.Flying)
end)

local flyingCheckboxText = flyingCheckbox:CreateFontString(nil, "ARTWORK", "GameFontNormal")
flyingCheckboxText:SetPoint("LEFT", flyingCheckbox, "RIGHT", 5, 0)
flyingCheckboxText:SetText("Flying")

-- Nameplate slider
local nameplateSlider = CreateFrame("Slider", "NameplateSlider", ui, "OptionsSliderTemplate")
nameplateSlider:SetPoint("TOPLEFT", flyingCheckbox, "BOTTOMLEFT", 5 , -10)
nameplateSlider:SetSize(150, 20)
nameplateSlider:SetMinMaxValues(0, 200)
nameplateSlider:SetValueStep(1)
nameplateSlider:SetValue(state.NameplateDistance)
nameplateSlider:SetScript("OnValueChanged", function(self, value)
    state.NameplateDistance = value
    api.SetNameplateDistanceMax(value)
end)

local nameplateSliderText = nameplateSlider:CreateFontString(nil, "ARTWORK", "GameFontNormal")
nameplateSliderText:SetPoint("TOP", nameplateSlider, "BOTTOM", 0, 0)
nameplateSliderText:SetText("Nameplate Distance")

-- Camera slider
local cameraSlider = CreateFrame("Slider", "CameraSlider", ui, "OptionsSliderTemplate")
cameraSlider:SetPoint("TOPLEFT", nameplateSlider, "BOTTOMLEFT", 0, -15)
cameraSlider:SetSize(150, 20)
cameraSlider:SetMinMaxValues(30, 150)
cameraSlider:SetValueStep(1)
cameraSlider:SetValue(state.CameraDistance)
cameraSlider:SetScript("OnValueChanged", function(self, value)
    state.CameraDistance = value
    api.SetCameraDistanceMax(value)
    api.SetCVarEx("cameraDistanceMaxZoomFactor", 100 + value)
end)

local cameraSliderText = cameraSlider:CreateFontString(nil, "ARTWORK", "GameFontNormal")
cameraSliderText:SetPoint("TOP", cameraSlider, "BOTTOM", 5, 0)
cameraSliderText:SetText("Camera Distance")

-- Create the climbing slider
local climbingSlider = CreateFrame("Slider", "ClimbingSlider", ui, "OptionsSliderTemplate")
climbingSlider:SetPoint("TOPLEFT", cameraSlider, "BOTTOMLEFT", 0, -15)
climbingSlider:SetSize(150, 20)
climbingSlider:SetMinMaxValues(60, 90)
climbingSlider:SetValueStep(1)
climbingSlider:SetValue(state.Climbing)
climbingSlider:SetScript("OnValueChanged", function(self, value)
    state.Climbing = value

    -- Convert from rad to deg
    value = math.rad(value)
    api.SetClimbAngle(value)
end)

local climbingSliderText = climbingSlider:CreateFontString(nil, "ARTWORK", "GameFontNormal")
climbingSliderText:SetPoint("TOP", climbingSlider, "BOTTOM", 5, 0)
climbingSliderText:SetText("Climbing Angle")

-- Minimap Button
local f = CreateFrame("Button", nil, Minimap)
f:SetFrameLevel(8)
f:SetSize(25,25)
f:SetMovable(true)
f:SetNormalTexture("Interface\\MacroFrame\\MacroFrame-Icon")
f:SetPushedTexture("Interface\\MacroFrame\\MacroFrame-Icon")
f:SetHighlightTexture("Interface\\MacroFrame\\MacroFrame-Icon")
local myIconPos = 0
local function UpdateMapBtn()
    local Xpoa, Ypoa = GetCursorPosition()
    local Xmin, Ymin = Minimap:GetLeft(), Minimap:GetBottom()
    Xpoa = Xmin - Xpoa / Minimap:GetEffectiveScale() + 70
    Ypoa = Ypoa / Minimap:GetEffectiveScale() - Ymin - 70
    myIconPos = math.deg(math.atan2(Ypoa, Xpoa))
    f:ClearAllPoints()
    f:SetPoint("TOPLEFT", Minimap, "TOPLEFT", 52 - (80 * cos(myIconPos)), (80 * sin(myIconPos)) - 52)
end
f:RegisterForDrag("LeftButton")
f:SetScript("OnDragStart", function()
    f:StartMoving()
    f:SetScript("OnUpdate", UpdateMapBtn)
end)
f:SetScript("OnDragStop", function()
    f:StopMovingOrSizing();
    f:SetScript("OnUpdate", nil)
    UpdateMapBtn();
end)
f:ClearAllPoints();
f:SetPoint("TOPLEFT", Minimap, "TOPLEFT", 52 - (80 * cos(myIconPos)),(80 * sin(myIconPos)) - 52)
f:SetScript("OnClick",
        function()
            if not ui:IsVisible() then
                ui:Show()
            else
                ui:Hide()
            end
        end
)

